#!/usr/bin/env bash
public_ip=$(cd terraform && terraform output --json | jq ".ip.value")
public_ip=$(sed -e 's/^"//' -e 's/"$//' <<<"$public_ip")

cp ansible/inventory.ini.dist ansible/inventory.ini
sed -i "s/{public_ip}/$public_ip/g" ansible/inventory.ini