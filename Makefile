.PHONY: ansible up down

up: terraform-up terraform-to-env ansible
down: terraform-down

ansible:
	@./scripts/run_ansible.sh -i ansible/inventory.ini ansible/initiate_runner.yaml

terraform-up:
	@cd terraform && terraform apply -auto-approve

terraform-to-env:
	@./scripts/terraform_to_env.sh

terraform-down:
	@cd terraform && terraform destroy -auto-approve