output "ip" {
  value = aws_instance.gitlab_runner.public_ip
}