provider "aws" {
  region = "eu-central-1"
  profile = "gitlab_runner"
}