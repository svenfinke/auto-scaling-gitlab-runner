resource "aws_instance" "gitlab_runner" {
  ami = "ami-0cc0a36f626a4fdf5" # Ubuntu 18.04 64-Bit x86, Frankfurt
  instance_type = "t2.micro"
  associate_public_ip_address = true
  key_name = aws_key_pair.public_key.key_name
  tags = {
    Name = "GitlabRunner"
  }
}

resource "aws_key_pair" "public_key" {
  key_name = "svenfinke-public"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCq+CFzxStj0OBU4yEZaq4D/IBdnX7djxvK7IeTtuRi2wpaOHOqXdidlALkpWHqsCuqYIIHtz0lTmciSMblb/Ch2cr5SLXijDkl+CDaeTsWhA/H+Sg5QYn7QpQyZ1aXzpEqGW5+e95MTLGeOcdE9yo5RLEbx/kTuqP/rzyd0HbarsbrPX9bh9FXSfFs2o98N4rEv0pfTIlQxnQYbw/3BFplPyO9SXrtF+ADXd+YkDjwlrBucBj5lX7QpU7dRoSiAs7+DAnVoiB35pO4WgNBue4gWg/btK5OjpreA4sAhiAFuozvWTVl6+xB8Eu706AVlV8k0tP34W1zBQ3NKPq+eV0r svenfinke@Svens-MacBook-Pro.local"
}